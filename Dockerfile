FROM python:3.9

MAINTAINER vhuezo 

WORKDIR /app

RUN apt-get update && apt-get install -y --no-install-recommends postgresql-client curl nano  iputils-ping && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /app/

RUN pip install --no-cache-dir -r requirements.txt
#RUN pip install micropython-msilib


COPY . /app

EXPOSE 8383
CMD ["python", "manage.py", "runserver", "0.0.0.0:8383"]


# CMD ["python", "gestion.py"]
