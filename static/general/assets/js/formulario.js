/*===============wizard-picture ====================*/
// Prepare the preview for profile picture
$("#wizard-picture").change(function(){
    readURL(this);
});
$('[data-toggle="wizard-radio"]').click(function(){
    wizard = $(this).closest('.wizard-card');
    wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
    $(this).addClass('active');
    $(wizard).find('[type="radio"]').removeAttr('checked');
    $(this).find('[type="radio"]').attr('checked','true');
});

$('[data-toggle="wizard-checkbox"]').click(function(){
    if( $(this).hasClass('active')){
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
    } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked','true');
    }
});

$('.set-full-height').css('height', 'auto');
/*===============wizard-picture ====================*/

/*===============iconEye ====================*/

const iconEye = document.querySelector(".icon-eye");
console.log(iconEye)

iconEye.addEventListener("click", function(){
    console.log(this.nextElementSibling.type);
})

/*===============iconEye ====================*/