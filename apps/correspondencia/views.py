from django.shortcuts import render
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required(login_url='/ingrasar/')
def inbox(request):
    usuario = request.user
    nombre=request.user.username
    context = {
        'usuario':usuario,
        'nombre':nombre,
        'titulo':'SE | INBOX CORRESPONDENCIA'
    }
    return render(request,'gestion/correspondencia/Inbox.html',context)
