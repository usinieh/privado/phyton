from django.db import models

# Create your models here.

#===========================================================================================
#|                           MODELO DEPARTAMENTO (UBICACION)                               |
#===========================================================================================

class Departamento(models.Model):
    codigo = models.CharField(
        max_length=2, unique=True, help_text=u'Código Oficial del Departamento')
    nombre = models.CharField(
        max_length=128, unique=True, help_text='Nombre del Departamento')

    def __unicode__(self):
        return u'%s' % (self.nombre)

    def __str__(self):
        return self.nombre 
    

    # para el admin
    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'

#===========================================================================================
#|                            MODELO MUNICIPIO (UBICACION)                                 |
#===========================================================================================
class Municipio(models.Model):
    codigo = models.CharField(
        max_length=2, help_text=u'Código Oficial del municipio en el departamento')
    nombre = models.CharField(max_length=128, help_text='Nombre del Municipio')
    departamento = models.ForeignKey(
        Departamento, on_delete=models.CASCADE, help_text='Departamento en el cual se encuentra ubicado')

    def __str__(self):
        return self.nombre 

    def natural_key(self):
        return u'%s | %s' % (self.codigo, self.nombre)

    def __unicode__(self):
        if (len(str(self.codigo)) < 2):
            codigo = '0' + str(self.codigo)
        else:
            codigo = str(self.codigo)

        return u'' + codigo + ' | ' + self.nombre

    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'
        unique_together = (("codigo", "departamento"),)

#===========================================================================================
#|                              MODELO ALDEA (UBICACION)                                   |
#===========================================================================================
class Aldea(models.Model):
    codigo = models.CharField(
        max_length=3, help_text=u'Código Oficial de la aldea')
    nombre = models.CharField(max_length=128, help_text=u'Nombre de la Aldea')
    municipio = models.ForeignKey(Municipio, on_delete=models.CASCADE,
                                  help_text=u'Municipio donde se encuentra ubicada la aldea')

    def __str__(self):
        return self.nombre 

    def natural_key(self):
        return u'%s | %s' % (self.codigo, self.nombre)

    def __unicode__(self):
        if (len(str(self.codigo)) < 2):
            codigo = '00' + str(self.codigo)
        else:
            if (len(str(self.codigo)) < 3):
                codigo = '0' + str(self.codigo)
            else:
                codigo = str(self.codigo)

        return u'%s | %s ' % (codigo, self.nombre)

    class Meta:
        verbose_name = 'Aldea'
        verbose_name_plural = 'Aldeas'
        #unique_together = (("codigo", "municipio"),)