from django.urls import path
from . import views
app_name = 'general'

urlpatterns = [

#===========================================================================================
#|                              URL DE PANTALLA PRINCIPAL LOGEADO                          |
#===========================================================================================

    path('principal', views.principal, name= 'principal'),


#===========================================================================================
#|                                   URLS DE AJAX                                          |
#===========================================================================================

    path('ajaxmun', views.ajaxmun, name= 'ajaxmun'),
    path('ajaxald', views.ajaxald, name= 'ajaxald'),
]
