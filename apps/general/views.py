from django.shortcuts import render
from django.contrib.auth.decorators import login_required


from apps.general.models import *
from django.shortcuts import HttpResponse
import json

# Create your views here.


#===========================================================================================
#|                             VISTA DE PRINCIPAL LOGEADO                                  |
#===========================================================================================

@login_required(login_url='/ingrasar/')
def principal(request):
    usuario = request.user
    nombre=request.user.username
    context = {
        'usuario':usuario,
        'nombre':nombre,
        'titulo':'SE | HOME'
    }
    return render(request,'general/home.html',context)

#===========================================================================================
#|                                    VISTAS DE AJAXS                                      |
#===========================================================================================

#___________________________________________________________________________________________
#|                                       MUNICIPIO                                         |
#-------------------------------------------------------------------------------------------


def ajaxmun(request):
    print(request)
    if request.accepts('text/html'):
        if request.GET.get('data', '') == 'municipios':
            mun = Municipio.objects.filter(departamento_id=request.GET['dep']).values('pk', 'codigo', 'nombre').order_by('pk')
            data = [{'id': m['pk'], 'codigo': m['codigo'], 'nombre': m['nombre']} for m in mun]
            return HttpResponse(json.dumps(data), content_type="application/json")

#___________________________________________________________________________________________
#|                                       ALDEA                                             |
#-------------------------------------------------------------------------------------------
def ajaxald(request):
   if request.accepts('text/html'):
      if request.GET.get('data', '') == 'aldeas':
         ald = Aldea.objects.filter(municipio_id=request.GET['mun']).values('pk', 'codigo', 'nombre').order_by('pk')
         data = [{'id': a['pk'], 'codigo': a['codigo'], 'nombre': a['nombre']} for a in ald]
         return HttpResponse(json.dumps(data), content_type="application/json")


