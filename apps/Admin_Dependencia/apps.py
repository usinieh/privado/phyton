from django.apps import AppConfig


class AdminDependenciaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.Admin_Dependencia'
