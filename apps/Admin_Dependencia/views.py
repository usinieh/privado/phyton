from django.shortcuts import render, redirect
from django.utils.dateparse import parse_date
from django.contrib.auth.decorators import login_required, permission_required
from datetime import date,datetime
from django.contrib import messages

#importando las clases del aplicativo
from apps.general.models import Departamento,Aldea,Municipio
from apps.Admin_Unidad.models import *
from .models import *
from .form import *

from django.contrib.auth.models import User, Group

# Create your views here.


#===========================================================================================
#===========================================================================================
#||                                                                                       ||
#||                                    DEPENDENCIA                                        ||
#||                                                                                       ||
#===========================================================================================
#===========================================================================================

#--------------------------------------------------------------------------------------------

#===========================================================================================
#|                            VISTA LISTAR DEPENDENCIA                                     |
#===========================================================================================
 


def ver_dependencia(request):
   usuario = request.user
   userid=request.user.id
   #dependencia = Dependencia.objects.get(usuario_creador_id=userid)
   dependencia = Dependencia.objects.all()
       
   context={
       'dep': dependencia,
       'usuario': userid,
        'titulo': ' SE | Listar dependencia' }
   return render(request, 'gestion/dependencia/adminDependenciaList.html', context) 

#===========================================================================================
#|                              VISTA FORMULARIO (CLASE)                                   |
#===========================================================================================

@login_required
def formulario(request):
    dep= DependenciaForm()
    context={
        'form': dep
    }
    return render(request,'gestion/dependencia/adiminDependencia.html', context )

#===========================================================================================
#|                                GUARDAR FORMULARIO                                         |
#===========================================================================================
def procesar_formulario(request):
    usuario = request.user
    dependencia= DependenciaForm()
    if request.method == 'POST':
        dependencia= DependenciaForm(request.POST)
        if dependencia.is_valid():
            formulario = dependencia.save(commit=False)
            # si no estableces el estado activo en el form de arriba tambien puedes ponerlo aqui o modificar otro campo que tu quieras
            formulario.usuario_creador = usuario
            
            formulario.save()
            # nombre_dependencia=dependencia.cleaned_data.get("nombre")
            messages.success(request, 'dependencia {nombre_dependencia} creada')
            mensaje= 'Gueardado con exito'
            context={
            'dep': dependencia,
            'usuario': usuario,
            'mensaje':'dependencia creada Correctamente',
            'tipo':'success',
            'icon':'check',
            }
            #return render(request,'gestion/dependencia/adminDependenciaList.html', context )
            return redirect('/dependencia/listar')
        else:
            mensaje= 'formulario no valido'
            context={
                'dep': dependencia,
                'usuario': usuario,
                'mensaje':'hay un problema con los datos',
                'tipo':'warning',
                'icon':'circle-exclamation',
                }
                
            return render(request,'gestion/dependencia/adiminDependencia.html', context )

            #for msg in dependencia.error_messages:
            # messages.error(request, 'No se logro guardar.')
                    #messages.add_message(request, messages.ERROR, 'No se logro guardar.')

    context={
            'dep': dependencia,
            'usuario': usuario,
            'mensaje':'No se pudo crear la dependencia',
            'tipo':'danger',
            'icon':'octagon-xmark',
            }
    return render(request,'gestion/dependencia/adminDependenciaList.html', context )



#===========================================================================================
#|                              VISTA INDIVIDUAL DEPENDENCIA                                    |
#===========================================================================================
@login_required
def individual_dependencia(request, id):
    dependencia = Dependencia.objects.filter(id=id)
    data = {
        'titulo': ' SE |  Dependencia Individual', 
        'dep': dependencia,
    }

    return render(request, "gestion/dependencia/adminDependenciaView.html", data)


#===========================================================================================
#|                   VISTA EDICION DEPENDENCIA  (cargando datos en plantilla)                   |
#===========================================================================================
@login_required
def edicion_dependencia(request, id):
    dependencia = Dependencia.objects.get(id=id)
    uni = RegistroUnidad.objects.all()
    usuario = request.user
    form= DependenciaForm()

#------------------Departamento ---------------------------------
    depa=Departamento.objects.all()

#------------------- Municipio------------------------------------
    mun= Municipio.objects.all()

#------------------- aldea ------------------------------------
    ald= Aldea.objects.all()

#------------------- Año  Funcionamiento --------------------------
    fun= dependencia.en_funcionamiento_anio
    anio=fun.strftime("%Y-%m-%d")

#------------------- en Funcionamiento --------------------------
    if dependencia.en_funcionamiento <= 1:
        efun= 'SI'
    else:
        efun= 'NO'

    data = {
        'titulo': ' SE | Editando Dependencia', 
        'form': form,
        'dep': dependencia,
        'uni': uni,
        'depa':depa,
        'mun':mun,
        'ald':ald,
        'anio':anio,
        'usuario': usuario,
        'efun':efun,
    }
    return render(request, "gestion/dependencia/adiminDependencia_editar.html", data)
    

#===========================================================================================
#|                              VISTA EDITAR DEPENDENCIA                                        |
#===========================================================================================
@login_required   
def editar_dependencia(request):
    dependencia= Dependencia.objects.all()
    depas=Departamento.objects.all()

    if request.method == 'POST':
        #unidad= UnidadForm(request.POST)
        id =  int(request.POST.get('id'))
        nombre = request.POST.get('nombre')
        tel_fijo = request.POST.get('tel_fijo')
        extension = request.POST.get('extension')
        abreviatura = request.POST.get('abreviatura')
        codigo = request.POST.get('codigo')
        departamento = int(request.POST.get('departamento'))
        municipio = int(request.POST.get('municipio'))
        aldea = int(request.POST.get('aldea'))
        en_funcionamiento = request.POST.get('en_funcionamiento')
        s = request.POST.get('en_funcionamiento_anio')
        unidad_pertenece =  int(request.POST.get('unidad_pertenece'))
        anio=parse_date(s)

        formLleno=Dependencia.objects.get(id=id)
        formLleno.nombre = nombre
        formLleno.tel_fijo = tel_fijo
        formLleno.extension = extension
        formLleno.abreviatura = abreviatura
        formLleno.codigo = codigo
        formLleno.departamento = Departamento.objects.get(id=departamento)
        formLleno.municipio = Municipio.objects.get(id=municipio)
        formLleno.aldea = Aldea.objects.get(id=aldea)
        formLleno.en_funcionamiento = en_funcionamiento
        formLleno.en_funcionamiento_anio = anio
        formLleno.usuario_modifico = User.objects.get( id= request.user.id)
        formLleno.fecha_modificacion=datetime.now()
        formLleno.unidad_pertenece=RegistroUnidad.objects.get(id=unidad_pertenece)
        formLleno.save()
        context={
            'titulo': ' SE |  Dependencia Editada', 
            'uni': dependencia,
            'dep': dependencia,
            'depa': depas,
            'mensaje':'  La Dependencia: '+nombre+' se a Modificado exitosamente!',
            'tipo':'success',
            'icon':'check'
         }
        return render(request,'gestion/dependencia/adminDependenciaList.html', context )