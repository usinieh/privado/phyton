from django.urls import path
from .views import *
from .models import *


app_name = 'Admin_Dependencia'

urlpatterns = [

#===========================================================================================
#|                               URLS DE DEPENDENCIA                                       |
#===========================================================================================

#------------------------------- Enlistar ----------------------------------------------    
    path('listar', ver_dependencia),


#------------------------------- Agregar  ----------------------------------------------  
    path('guardar', formulario),

#------------------------------- guardar  ----------------------------------------------  
    path('agregar', procesar_formulario, name= 'agregar'),

# #------------------------------- editar ------------------------------------------------ 
     path('editar', editar_dependencia),
     path('edicion/<int:id>', edicion_dependencia),

# #------------------------------- dar de baja ------------------------------------------- 
#     path('eliminar/<int:id>', eliminar_dependencia),

#---------------------------vista individual ---------------------------------- 
    path('individual/<int:id>', individual_dependencia),
]