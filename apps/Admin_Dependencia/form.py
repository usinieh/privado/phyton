#--------------------- IMPORTACIONES DJANGO------------------------------------
from cProfile import label
from dataclasses import field
from pyexpat import model

from django import forms
from turtle import textinput
from django.forms import ModelForm

#-------------------- IMPORTANDO ARCHIVOS DE APP ------------------------------
from .models import Dependencia
from apps.general.views import *

#===========================================================================================
#|                                  Formulario Dependencia                                      |
#===========================================================================================


class DependenciaForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     self.fields['usuario_creador'].widget = forms.HiddenInput()

    class Meta:
        model=Dependencia    
        #fields = '__all__'
        
        fields = [
            'codigo',
            'nombre',
            'abreviatura',
            'tel_fijo',
            'extension',
            'correo',
            'departamento',
            'municipio',
            'aldea',
            'en_funcionamiento',
            'en_funcionamiento_anio',
            'usuario_creador',
            'unidad_pertenece',
         ] 


        widget = {
             'usuario_creador':forms.HiddenInput(),
        }
