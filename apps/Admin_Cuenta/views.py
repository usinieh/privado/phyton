from codecs import unicode_escape_decode
from contextlib import nullcontext
#from msilib.schema import ListView
import pdb
from turtle import title
from django.utils.dateparse import parse_date
from datetime import datetime
from django.utils.datastructures import MultiValueDictKeyError

from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView
from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse_lazy
from django.contrib.auth.models import User, Group


from apps.general.models import *
from .form import CreateUserForm, CreatePersonaForm
from .models import *
from django.shortcuts import HttpResponse
import os
# Create your views here.


#===========================================================================================
#|                                Listar Usuarios-Personas                                 |
#===========================================================================================
@login_required 
def CntUer(request):
    usuario = request.user
    nombre=request.user.username
    usuario= User.objects.all()
    persona= Persona.objects.all()
    personaUsuario= PersonaUsuario.objects.all()

    context = {
        'usuario':usuario,
        'nombre':nombre,
        'titulo':'HOME',
        'persona': persona,
        'personaUsuario': personaUsuario,
        'titulo': ' SE | Control de Usuario '
    }
    return render(request,'gestion/usuario/controlUsuario.html',context)

#--------------------------------------     CLASE -----------------------------------------------
class UserListView(ListView):
    model: User
    template_name: 'gestion/usuario/controlUsuario.html'

    def dispatch(request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return User.objects.all()

    @method_decorator(login_required)
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context[title]= 'SE | Listado Usuarios'
        return context


#=================================== ADMINISTRACION GENERAL =====================================
def AdmGen(request):
    usuario = request.user
    nombre=request.user.username
    Dep = Departamento.objects.all()
    Mun = Municipio.objects.all()
    Ald = Aldea.objects.all()
    context = {
        'usuario':usuario,
        'nombre':nombre,
        'titulo':'SE | General',
        'departamento': Dep,
        'municipio': Mun,
        'aldea': Ald,
        'usuario': usuario,
        'titulo': ' SE | Control de Usuario '
        }
    return render(request,'gestion/general/adminGeneral.html',context)


def eliminaDepa(request, id):
    depa = Departamento.objects.get(id=id)
    depa.delete()

    return redirect('gestion/general/adminGeneral.html')  

def editarDepa(request, id):

    id = int(request.POST['id'])
    codigo = request.POST['txtCodigo']
    nombre = request.POST['txtNombre']

    depa = Departamento.objects.get(id=id)
    depa.nombre = nombre
    depa.codigo = codigo
    depa.save()

    return redirect('gestion/general/adminGeneral.html')

#===========================================================================================
#|                                Vista Registro Usuarios                                  |
#===========================================================================================

@login_required 
@permission_required('Admin_Cuenta.add_persona')
def registro(request):
    usuario = request.user
    user=CreateUserForm
    persona=CreatePersonaForm
    context={
        'form1': user,
        'form2': persona,
        'depa': Departamento.objects.all(),
        'mensaje':'hay un problema con los datos',
        'tipo':'warning' 
    }

    if request.method == 'POST':
       
        # pdb.set_trace()
        nombre1 = request.POST['primer_nombre']
        nombre2 = request.POST['segundo_nombre']
        apellido1 = request.POST['primer_apellido']
        apellido2 = request.POST['segundo_apellido']
        identidad = request.POST['identidad']
        direccion = request.POST['direccion']
        telefono = request.POST['telefono']
        sexo = request.POST['sexo']
        fecha_nacimiento = request.POST['fecha_nacimiento']
        correo = request.POST['correo_institucional']
        departamento = int(request.POST.get('departamento'))
        domicilio = int(request.POST.get('domicilio'))
        direccion = request.POST['direccion']
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        img=request.FILES.get('img')
        firma=request.FILES.get('firma')


        try:
            superuser = request.POST('is_superuser')
        except MultiValueDictKeyError:
            superuser = False
        #superuser = request.POST.get('is_superuser')
        #active = request.POST.get('is_active')
        try:
            active= request.POST('is_active')
        except MultiValueDictKeyError:
            active = False
        #staff = request.POST.get('is_staff')
        try:
            staff= request.POST('is_staff')
        except MultiValueDictKeyError:
            staff = False

        mun=Municipio.objects.get(id=domicilio)

        if superuser == 'on':
            superuser=True
        else:
            superuser=False

        if active== 'on':
            active=True
        else:
            active=False

        if staff== 'on':
            staff=True
        else:
            staff=False
        
        nombres= nombre1 +' '+ nombre2
        apellidos= apellido1+ ' '+ apellido2

        personaForm= Persona.objects.create(
            identidad=identidad,
            primer_nombre=nombre1,
            segundo_nombre=nombre2,
            primer_apellido=apellido1,
            segundo_apellido=apellido2,
            domicilio=mun,
            direccion=direccion,
            telefono=telefono,
            sexo=sexo,
            fecha_nacimiento=fecha_nacimiento,
            correo_institucional=correo,
            correo=email,
            usuario_creador=usuario,
            img=img,
            firma=firma
        )
        personaForm.save()
        # Guardar Usuario
        usuarioForm = User.objects.create(
            username=username,
            email=correo,
            password=password2,
            first_name= nombres,
            last_name= apellidos,
            is_superuser=superuser,
            is_active=active,
            is_staff=staff
        )
        usuarioForm.save()

        # Guardaer el usuario por persona
        personausuario_crear = PersonaUsuario.objects.create(
            persona=personaForm,
            usuario=usuarioForm,

        )

        #------------GUARDAR DATOS-----------------
        #usuarios_crear.groups.add(Group.objects.get(name = 'Concursante Docente'))
        
        personausuario_crear.save()

        #-------------PERMISOS ----------------------
        try:
            uni = request.POST['is_unidad']
        except MultiValueDictKeyError:
            uni = False

        try:
            dep = request.POST['is_dependencia']
        except MultiValueDictKeyError:
            dep = False

        if uni == 'on':
            usuarioForm.groups.add(Group.objects.get(name = 'unidad'))

        if dep == 'on':
            usuarioForm.groups.add(Group.objects.get(name = 'dependencia'))

        nombre=request.user.username
        persona= Persona.objects.all()
        personaUsuario= PersonaUsuario.objects.all()

        context = {
            'usuario':usuario,
            'nombre':nombre,
            'titulo':'HOME',
            'persona': persona,
            'personaUsuario': personaUsuario,
            'mensaje':'se guardo Correctamente',
            'tipo':'success',
            'icon':'check',
        }
        return render(request,'gestion/usuario/controlUsuario.html',context)
        # return render(request,CntUer,context)
    
    return render(request,'gestion/usuario/crearUsuario.html',context )

#===========================================================================================
#|                              VISTA EDITAR USUARIO                                        |
#===========================================================================================

def editar_cuenta(request, idPer, idUser,idUsePer):
    persona = Persona.objects.get(id=idPer)
    user= User.objects.get(id=idUser)
    perUser= PersonaUsuario.objects.get(id=idUsePer)

    depas=Departamento.objects.all()

    #----------------- Genero ------------------------------
    gen= persona.get_sexo_display()

    #------------- Departamento y domicilio ------------------
    dom= persona.domicilio_id
    m = Municipio.objects.get(id=dom) 
    d = Departamento.objects.get(id=m.departamento_id) 

    #----------------------Permisos ------------------------
    if user.is_superuser == True:
        estado1='checked'
    else:
        estado1=''

    if user.is_active== True:
        estado2='checked'
    else:
        estado2=''

    if user.is_staff== True:
        estado3='checked'
    else:
        estado3=''

    #------------------- Fecha Nacimiento --------------------------
    fun= persona.fecha_nacimiento
    anio=fun.strftime("%Y-%m-%d")


    context={
        'persona': persona,
        'usuario': user,
        'perUser': perUser,
        'depa': depas,
        'genero' : gen,
        'd':d, 'm':m,'anio':anio,
        'est1':estado1, 'est2':estado2, 'est3':estado3,
        'mensaje':'hay un problema con los datos',
        'tipo':'warning', 
        'titulo':'SE | Editar Usuario',
    }

    return render(request, "gestion/usuario/editarUsuario.html", context)
#===========================================================================================
#|                              VISTA EDICION USUARIO                                      |
#===========================================================================================
@login_required   
def edicion_cuenta(request):
    usuario = request.user
    context={
        'mensaje':'hay un problema con los datos',
        'tipo':'warning', 
    }

    if request.method == 'POST':
        idUser =  int(request.POST.get('idUser'))
        idPer =  int(request.POST.get('idPer'))
        idUsPeer =  int(request.POST.get('idUsPeer'))
        nombre1 = request.POST['primer_nombre']
        nombre2 = request.POST['segundo_nombre']
        apellido1 = request.POST['primer_apellido']
        apellido2 = request.POST['segundo_apellido']
        identidad = request.POST['identidad']
        direccion = request.POST['direccion']
        telefono = request.POST['telefono']
        sexo = request.POST['sexo']
        fecha_nacimiento = request.POST['fecha_nacimiento']
        correo = request.POST['correo']
        domicilio = int(request.POST.get('domicilio'))
        direccion = request.POST['direccion']
        username = request.POST['username']
        email = request.POST['email']
        img=request.FILES.get('img')
        img2=request.FILES.get('img2')
        img3=request.FILES.get('img3')
        firma=request.FILES.get('firma')
        
        anio=parse_date(fecha_nacimiento)
        
        #------------- PERMISOS USUARIO----------------
        try:
            superuser = request.POST.get('is_superuser')
        except MultiValueDictKeyError:
            superuser = False
        #superuser = request.POST.get('is_superuser')
        #active = request.POST.get('is_active')
        try:
            active= request.POST.get('is_active')
        except MultiValueDictKeyError:
            active = False
        #staff = request.POST.get('is_staff')
        try:
            staff= request.POST.get('is_staff')
        except MultiValueDictKeyError:
            staff = False
        

        if superuser != 'on':
            superuser=False
            
            print('no es superuser')
        else:
            superuser=True
            print('si es superuser')

        if active != 'on':
            print('no es active')
        else:
            active=True
            print('si es active')

        if  staff != 'on':
            staff=False
            print('no es staff')
        else:
            staff =True
            print('si es staff')

        #------------------- IMAGENES -----------------

        per= Persona.objects.get(id=idPer)
        fi = per.firma
        fo = per.img

        if img == '':
            imfUser= img2
            if fi != imgFirma:
                imagen = per.firma
            try:
                os.remove(imagen.path)
            except Exception as e:
                pass
        if firma == '':
            imgFirma= img3
            if fo != imfUser:
                imagen2 = per.img
            try:
                os.remove(imagen2.path)
            except Exception as e:
                pass    

       
        if fi != firma:
            imagen = per.firma
            try:
                os.remove(imagen.path)
            except Exception as e:
                pass
        
        if fo != img:
            imagen2 = per.img
            try:
                os.remove(imagen2.path)
            except Exception as e:
                pass


        #------------ INICIO GUARDADO----------------------------

        formUsuario= User.objects.get(id=idUser)
        formUsuario.username= username
        formUsuario.email= email
        formUsuario.save()

        formPersona= Persona.objects.get(id=idPer)
        formPersona.identidad = identidad
        formPersona.primer_nombre = nombre1
        formPersona.segundo_nombre = nombre2
        formPersona.primer_apellido = apellido1
        formPersona.segundo_Apellido = apellido2
        formPersona.sexo = sexo
        formPersona.fecha_nacimiento= anio
        formPersona.domicilio= Municipio.objects.get(id=domicilio)
        formPersona.direccion= direccion
        formPersona.telefono= telefono
        formPersona.correo_institucional= correo
        formPersona.usuario_modifico= User.objects.get( id= request.user.id)
        formPersona.fecha_modificacion = datetime.now()
        if img == '':
            formPersona.img= img2
        else:
            formPersona.img= img
        if firma == '':
            formPersona.firma= firma
        else:
            formPersona.firma= firma
        
        formPersona.save()

        fromUserPerson= PersonaUsuario.objects.get(id=idUsPeer)
        fromUserPerson.usuario =formUsuario
        fromUserPerson.persona = formPersona
        fromUserPerson.is_superuser=superuser
        fromUserPerson.is_active=active
        fromUserPerson.is_staff=staff
        fromUserPerson.save()

        nombre=request.user.username
        persona= Persona.objects.all()
        personaUsuario= PersonaUsuario.objects.all()

        context = {
            'usuario':usuario,
            'nombre':nombre,
            'titulo':'HOME',
            'persona': persona,
            'personaUsuario': personaUsuario,
            'mensaje':'se edito Correctamente',
            'tipo':'success',
            'icon':'check',
        }
        return render(request,'gestion/usuario/controlUsuario.html',context)
    
    return render(request,'gestion/usuario/editarUsuario.html',context )

#===========================================================================================
#|                              VISTA INDIVIDUAL UNIDAD                                    |
#===========================================================================================
@login_required
def individual_cuenta(request, id):
    usuario = PersonaUsuario.objects.filter(id=id)
    data = {
        'titulo': ' SE |  Unidad Individual', 
        'usuario': usuario,
    }

    return render(request, "gestion/usuaio/adminUsuarioView.html", data)
