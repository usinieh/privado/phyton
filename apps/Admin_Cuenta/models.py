from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
#auth.User.user_permissions
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from ..general.models import *
from .choices import *
from .managers import UserManager

# Create your models here.
#===========================================================================================
#|                                   Guardado de archivos                                  |
#===========================================================================================
def perfil(obj, filename):
    ext = str(obj.identidad).split('.')
    ruta = "usuario/imagenes/perfil/%s.%s"%(obj.identidad,ext[-1])
    return ruta

def firma(obj, filename):
    ext = str(obj.identidad).split('.')
    ruta = "usuario/imagenes/firmas/%s.%s"%(obj.identidad,ext[-1])
    return ruta

#===========================================================================================
#|                                     MODELO PERSONA                                      |
#===========================================================================================
class Persona(models.Model):
    identidad = models.CharField( max_length=20, help_text=u'Numero único de identificación', verbose_name=u'Identificación')
    primer_nombre = models.CharField(max_length=60, help_text='Primer nombre', verbose_name='Primer Nombre', db_index=True)
    segundo_nombre = models.CharField(max_length=60, help_text='Segundo nombre',null=True, blank=True, verbose_name='Segundo Nombre')  # no requerido
    primer_apellido = models.CharField(max_length=60, help_text='Primer apellido', verbose_name='Primer Apellido', db_index=True)
    segundo_apellido = models.CharField(max_length=60, help_text='Segundo apellido', null=True, blank=True, verbose_name='Segundo Apellido')  # no requerido
    sexo = models.CharField(max_length=1, choices=SEXOS, help_text='Sexo')
    fecha_nacimiento = models.DateTimeField(help_text='Fecha de nacimiento', verbose_name='Fecha de Nacimiento')
    domicilio = models.ForeignKey(Municipio, on_delete=models.CASCADE,help_text='Domicilio actual de la persona', verbose_name="Municipio", null=True, blank=True)
    direccion = models.CharField(max_length=200, help_text=u'Dirección exacta de la persona', null=True, blank=True)
    telefono = models.CharField(max_length=9, help_text=u'Teléfono de domicilio de la persona',null=True, blank=True, verbose_name=u'Teléfono')  # No requerido
    lugar_nacimiento = models.CharField(null=True, blank=True, max_length=150, help_text='Lugar de Nacimiento', verbose_name=u'Lugar de Nacimiento')
    correo = models.EmailField(null=True, blank=True, help_text=u'Correo electrónico que actualmente utiliza', verbose_name=u'Correo Electrónico')
    correo_institucional = models.EmailField(null=True, blank=True, help_text=u'Correo electrónico asignado por la SEDUC', verbose_name=u'Correo Electrónico Institucional')
    usuario_creador = models.ForeignKey(User, on_delete=models.CASCADE, related_name='persona_creador', null=True, blank=True)
    usuario_modifico = models.ForeignKey(User, on_delete=models.CASCADE, related_name='persona_modificador', null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, null=True, blank=True)
    img = models.ImageField(upload_to=perfil, verbose_name="Archivo(Max:500MB)",null=True, blank=True)
    firma = models.ImageField(upload_to=firma, verbose_name="Archivo(Max:500MB)",null=True, blank=True)

    def __unicode__(self):
        completo = self.primer_nombre
        if self.segundo_nombre:
            completo = completo + ' ' + self.segundo_nombre
        completo = completo + ' ' + self.primer_apellido
        if self.segundo_apellido:
            completo = completo + ' ' + self.segundo_apellido
        return u'%s' % (completo)

    def first_names(self):
        completo = self.primer_nombre
        if self.segundo_nombre:
            completo = completo + ' ' + self.segundo_nombre
        return u'%s' % (completo)

        # def existe_persona(self, tipo_id, identidad):
        #     if Persona.objects.filter(tipoID=tipo_id, identidad=identidad):
        #         return True
        #     else:
        #         return False

    def get_foreign_fields(self):
        return [getattr(self, f.name) for f in self._meta.fields if type(f) == models.fields.related.ForeignKey]

    class Meta:
        unique_together = (("identidad"),)

#===========================================================================================
#|                            MODELO PERSONA POR USUARIO                                   |
#===========================================================================================
class PersonaUsuario(models.Model):
    persona = models.OneToOneField(Persona, on_delete=models.CASCADE, related_name='persona_usuarios')
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, unique=True, related_name='usuario_persona')
    fecha = models.DateField(auto_now_add=True)
    login_count = models.PositiveIntegerField(default=0)

    class Meta:
        unique_together = (('persona', 'usuario'), )

    def __unicode__(self):
        return '%s' % self.usuario.username

    # def login_userc(sender, user, request, **kwargs):
    #     identidad = request.user.username[:13]
    #     per = get_object_or_404(PersonaUsuario,on_delete=models.CASCADE, usuario=request.user, persona__identidad=identidad)
    #     per.login_count = per.login_count + 1
    #     per.save()

    #     user_logged_in.connect(login_userc)

