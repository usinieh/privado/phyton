from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Aldea)
admin.site.register(Departamento)
admin.site.register(Municipio)
admin.site.register(Persona)
admin.site.register(PersonaUsuario)

#@admin.register(Curso)
# class CursoAdmin(admin.ModelAdmin):
#     # pass
#     # que muestre esos datos como tabla
#     list_display = ('id', 'coloreado', 'creditos')
#     # ordering= ('- nombre',)# ordenar los datos de tabla de a-z
#     # search_fields=('nombre','creditos')#filtro de busqueda parte superior de tabla
#     # list_editable=('nombre','creditos')# que el campo sea editable, boton guardar al final
#     # list_display_links=('nombre',)##que ese campo sea hipervinculo
#     # list_filter=('creditos',)## agregar el panel de filtro a la izquierda
#     # list_per_page=3 ##enlistar solo 3 y mostrar filtro para ver los siguientes
#     # exclude=('creditos',)##no mostrar los creditos
#     '''
#     fieldsets = (
#     (None, {
#         'fields':('nombre',)
#     }),
#     ('Advanced options',{
#         'classes': ('collapse', 'wide', 'extrapretty'),
#         'fields': ('creditos',)
#     })
#     )
#     '''

#     def datos(self, obj):
#         return obj.nombre.upper()

#     datos.short_description = "CURSO (MAYUS)"
#     datos.empty_value_display = "???"
#     datos.admin_order_field = "nombre"


#admin.site.register(Curso, CursoAdmin)
#admin.site.register(Docente)