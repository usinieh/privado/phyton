from django.apps import AppConfig


class AdminCuentaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.Admin_Cuenta'
