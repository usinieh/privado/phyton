
#--------------------- IMPORTACIONES DJANGO------------------------------------
from cProfile import label
from django.contrib.admin.widgets import AdminDateWidget,AdminTextInputWidget
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

#-------------------- IMPORTANDO ARCHIVOS DE APP ------------------------------

from .models import *


#===========================================================================================
#|                                  Formulario Persona                                     |
#===========================================================================================

class CreatePersonaForm(ModelForm):
    class Meta:
        model=Persona    
        
        fields = [
            'identidad',
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'sexo',
            'fecha_nacimiento',
            'domicilio',
            'direccion',
            'telefono',
            'correo_institucional',
            'usuario_creador',
            'img',
            'firma',
         ]

        labels={
            'identidad':'numero de identidad',
            'primer_nombre':'primer nombre',
            'segundo_nombre':'segundo nombre',
            'primer_apellido':'primer apellido',
            'segundo_apellido':'segundo apellidp',
            'sexo':'genero',
            'fecha_nacimiento':'fecha nacimiento',
            'domicilio':'municipio de su recidencia',
            'direccion':'Direccion exacta',
            'telefono':'telefono celular',
            'correo_institucional':'correo institucional',
            'usuario_creador':'usuario creador',
            'img':'imagen de usuario',
            'firma':'imagen firma de usuario',
        }
      

# #===========================================================================================
# #|                               Formulario crear usuario                                  |
# #===========================================================================================

class CreateUserForm(UserCreationForm):
    class Meta:
        model= User
        fields=[
            'username', 
            'email', 
            'password1', 
            'password2',
            'is_staff' ,
            'is_active' ,
            'is_superuser',
        ]

        labels={
            'username':'Nombre de usuario', 
            'email':'correo',
            'password1':'contraseña',
            'password2': 'contraseña confirmación',
             'is_staff': 'administrador' ,
            'is_active': 'usuario activo' ,
            'is_superuser': 'superusuario',
        }
     
