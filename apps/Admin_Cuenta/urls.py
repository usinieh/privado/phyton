from django.urls import path

from .views import *
#from apps.Admin_Cuenta.views import registroUsuarios

app_name = 'Admin_Cuenta'

urlpatterns = [
    #------------------------------- listar ----------------------------------------------  
    path('cuenta', CntUer, name= 'cuenta'),
    # path('general', views.AdmGen, name= 'general'),

    # #------------------------------- editar ------------------------------------------------ 
    path('edicion', edicion_cuenta),    
    path('editar/<int:idPer>,<int:idUser>,<int:idUsePer>', editar_cuenta),
    #------------------------------- guardar  ----------------------------------------------
    path('registrar', registro, name= 'registrar'),

    #---------------------------vista individual ---------------------------------- 
    path('individual/<int:id>', individual_cuenta),

]