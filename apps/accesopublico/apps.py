from django.apps import AppConfig


class AccesopublicoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.accesopublico'
