from encodings import normalize_encoding
from django.shortcuts import render, redirect
from .models import Comentario 

# Importaciones para login
from django.contrib.auth import authenticate, logout, login 
from django.contrib.auth.decorators import login_required

# Create your views here.
def vista(request):
    data = {
        'titulo': ' Secretaría de Educación, Gestión Interna '}
    return render(request,'index.html',data)

def ingrasar(request):
    if request.method == 'POST':
        username1 = request.POST.get('userneme')
        password1 = request.POST.get('password')

        user = authenticate(username=username1, password=password1)

        if user is not None:
            login(request, user)
            return redirect('/general/principal')
        else:
            
            #return redirect('/login')
            return redirect('/')

    data = {
        'titulo': ' Ingresar '}
        
    return render(request, 'ingreso/login.html',data)


def logoutUser(request):
    logout(request)
    return redirect('/')

def registroComentario(request):
    nombre = request.POST.get('nombre')
    correo = request.POST.get('correo')
    direccion = request.POST.get('direccion')
    telefono = request.POST.get('telefono')
    mensaje = request.POST.get('mensaje')

    Comentario.objects.create(
        nombre=nombre,
        correo=correo,
        direccion=direccion,
        telefono=telefono,
        mensaje=mensaje
    )
    context={
        'tipo':'success',
        'icon' : 'check',
        'mensaje' : 'Enviado con exito'
    }
    return render(request, '/', context)
