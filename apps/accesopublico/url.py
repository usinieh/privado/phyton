from django.urls import path
from . import views
app_name = 'accesopublico'

urlpatterns = [
    path('login', views.ingrasar, name= 'login'),
    #------------------------------- guardar comentario------------------------------------------
    path('enviar', views.registroComentario, name= 'enviar'),
]