from django.db import models

# Create your models here.


#===========================================================================================
#|                                   MODELO COMENTARIO                                     |
#===========================================================================================


class Comentario(models.Model):
    nombre = models.CharField(max_length=100)
    correo = models.EmailField(null=True, blank=True, help_text=u'Correo electrónico que actualmente utiliza', verbose_name=u'Correo Electrónico')
    direccion = models.CharField(max_length=200, help_text=u'Dirección exacta de la persona', null=True, blank=True)
    telefono = models.CharField(max_length=8, null=True, blank=True)
    mensaje = models.CharField(max_length=1000, help_text=u'Mensaje de la persona', null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __unicode__(self):
        nombreCompleto = "%s %s"%(self.nombres, self.apellidos)
        return nombreCompleto

    class Meta:
        verbose_name_plural = 'Comentario Publico'