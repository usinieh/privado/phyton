
#importaciones para las vistas de las clases
from multiprocessing import context
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, View
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.http import HttpRequest
from datetime import date,datetime
from django.utils.dateparse import parse_date
from django.contrib.auth.decorators import login_required, permission_required

#importando las clases del aplicativo
from apps.general.models import Departamento,Aldea,Municipio
from apps.Admin_Cuenta.choices import *
from .models import *
from .form import *

from django.contrib.auth.models import User, Group

# Create your views here.


#===========================================================================================
#===========================================================================================
#||                                                                                       ||
#||                                   UNIDAD                                              ||
#||                                                                                       ||
#===========================================================================================
#===========================================================================================

#--------------------------------------------------------------------------------------------

#===========================================================================================
#|                              VISTA FORMULARIO (FUNCION)                                 |
#===========================================================================================


# def UnidadCreateView(request):
#    form = UnidadForm
   
#    if request.method == 'POST':
#       form = UnidadForm(request.POST)
      
#       print (form)
#       if form.is_valid():
#             form.save()
#             return redirect('gestion/home.html')
#    context={'form':form, 'title':'SE | Crear Unidad'}
#    return render(request, 'gestion/unidad/adiminUnidad.html', context)


#===========================================================================================
#|                              VISTA FORMULARIO (CLASE)                                   |
#===========================================================================================

@login_required
def index(request):
    unidad= UnidadForm()
    context={
        'form': unidad
    }
    return render(request,'gestion/unidad/adiminUnidad.html', context )

#===========================================================================================
#|                                 GUARDAR FORMULARIO                                      |
#===========================================================================================
@login_required
def procesar_formulario(request):
    usuario = request.user
    unidad= UnidadForm()
    if request.method == 'POST':
        unidad= UnidadForm(request.POST)
        if unidad.is_valid():
            formulario = unidad.save(commit=False)
            # si no estableces el estado activo en el form de arriba tambien puedes ponerlo aqui o modificar otro campo que tu quieras
            formulario.usuario_creador = usuario

            formulario.save()
            # nombre_unidad=unidad.cleaned_data.get("nombre")
            # messages.success(request, 'Unida {nombre_unidad} creada')
            mensaje= 'Gueardado con exito'
            # context={
            # 'form': unidad,
            # 'usuario': usuario,
            # 'mensaje':'Unidad creada Correctamente',
            # 'tipo':'success',
            # 'icon':'check',
            # }
            messages.add_message(request, messages.SUCCESS, 'Unidad creada Correctamente.')
            return redirect('/unidad/listar')
        else:
            mensaje= 'formulario no valido'
            context={
                'form': unidad,
                'usuario': usuario,
                'mensaje':'hay un problema con los datos',
                'tipo':'warning',
                'icon':'circle-exclamation',
                }
                
            return render(request,'gestion/unidad/adiminUnidad.html', context )

            #for msg in unidad.error_messages:
            # messages.error(request, 'No se logro guardar.')
                    #messages.add_message(request, messages.ERROR, 'No se logro guardar.')

    context={
            'form': unidad,
            'usuario': usuario,
            'mensaje':'No se pudo crear la unidad',
            'mensaje':'mensaje'+mensaje,
            'tipo':'danger',
            'icon':'octagon-xmark',
            }
    return render(request,'gestion/unidad/adminDependenciaList.html', context )


#===========================================================================================
#|                              VISTA LISTAR UNIDAD                                        |
#===========================================================================================
 

@login_required
def ver_unidad(request):
   usuario = request.user
   userid=request.user.id
   #unidad = RegistroUnidad.objects.get(usuario_creador_id=userid)
   unidad = RegistroUnidad.objects.all()

    
#    uni = RegistroUnidad.objects.filter(usuario_creador = username).values('codigo','nombre','abreviatura','extension','tel_fijo','correo','en_funcionamiento')
#    #usuario_creador = historialdenuncia.objects.filter(denuncia__id_denuncia = username ).values('denuncia__id_denuncia','fecha_creacion', 'observacion', 'archivo', 'usuario__nombre')
#    initial_data= {
#       'unidad': unidad,
#    }
#   return redirect('/')
       
   context={
        'uni': unidad,
        'usuario': userid,
        'titulo': ' SE | Listar Unidad' }
   return render(request, 'gestion/unidad/adminUnidadList.html', context) 

#===========================================================================================
#|                   VISTA EDICION UNIDAD  (cargando datos en plantilla)                   |
#===========================================================================================
@login_required
def edicion_unidad(request, id):
    unidad = RegistroUnidad.objects.get(id=id)
    usuario = request.user
    form= UnidadForm()

#------------------Departamento ---------------------------------
    depa=Departamento.objects.all()

#------------------- Municipio------------------------------------
    mun= Municipio.objects.all()

#------------------- aldea ------------------------------------
    ald= Aldea.objects.all()

#------------------- Año  Funcionamiento --------------------------
    fun= unidad.en_funcionamiento_anio
    anio=fun.strftime("%Y-%m-%d")

#------------------- en Funcionamiento --------------------------
    efun= unidad.get_en_funcionamiento_display()
    # if unidad.en_funcionamiento >=1:
    #     efun= 'SI'
    # else:
    #     efun= 'NO'

    data = {
        'titulo': ' SE | Editando Unidad', 
        'form': form,
        'unidad': unidad,
        'depa':depa,
        'mun':mun,
        'ald':ald,
        'anio':anio,
        'usuario': usuario,
        'efun':efun,
    }
    return render(request, "gestion/unidad/adiminUnidad_editar.html", data)
    

#===========================================================================================
#|                              VISTA EDITAR UNIDAD                                        |
#===========================================================================================
@login_required   
def editar_unidad(request):
    unidad= RegistroUnidad.objects.all()
    depas=Departamento.objects.all()

    if request.method == 'POST':
        #unidad= UnidadForm(request.POST)
        id =  int(request.POST.get('id'))
        nombre = request.POST.get('nombre')
        tel_fijo = request.POST.get('tel_fijo')
        extension = request.POST.get('extension')
        abreviatura = request.POST.get('abreviatura')
        codigo = request.POST.get('codigo')
        departamento = int(request.POST.get('departamento'))
        municipio = int(request.POST.get('municipio'))
        aldea = int(request.POST.get('aldea'))
        en_funcionamiento = request.POST.get('en_funcionamiento')
        s = request.POST.get('en_funcionamiento_anio')
        anio=parse_date(s)

        formLleno=RegistroUnidad.objects.get(id=id)
        formLleno.nombre = nombre
        formLleno.tel_fijo = tel_fijo
        formLleno.extension = extension
        formLleno.abreviatura = abreviatura
        formLleno.codigo = codigo
        formLleno.departamento = Departamento.objects.get(id=departamento)
        formLleno.municipio = Municipio.objects.get(id=municipio)
        formLleno.aldea = Aldea.objects.get(id=aldea)
        formLleno.en_funcionamiento = en_funcionamiento
        formLleno.en_funcionamiento_anio = anio
        formLleno.usuario_modifico = User.objects.get( id= request.user.id)
        formLleno.fecha_modificacion=datetime.now()
        formLleno.save()
        context={
            'titulo': ' SE |  Unidad Editada', 
            'form': unidad,
            'uni': unidad,
            'depa': depas,
            'mensaje':'  La unidad: '+nombre+' se a Modificado exitosamente!',
            'tipo':'success',
            'icon':'check'
         }
        return render(request,'gestion/unidad/adminUnidadList.html', context )
 

#===========================================================================================
#|                              VISTA ELIMINAR UNIDAD                                      |
#===========================================================================================
@login_required
def eliminar_unidad(request, id):
    unidad = RegistroUnidad.objects.get(id=id)
    data = {
        'titulo': ' SE | Editando Unidad', 
        'usuario': unidad,
    }

    return render(request, "adiminUnidad_editar.html", data)


#===========================================================================================
#|                              VISTA INDIVIDUAL UNIDAD                                    |
#===========================================================================================
@login_required
def individual_unidad(request, id):
    unidad = RegistroUnidad.objects.filter(id=id)
    data = {
        'titulo': ' SE |  Unidad Individual', 
        'usuario': unidad,
    }

    return render(request, "gestion/unidad/adminUnidadView.html", data)

