#--------------------- IMPORTACIONES DJANGO------------------------------------
from cProfile import label
from dataclasses import field
from pyexpat import model
from django.contrib.admin.widgets import AdminDateWidget,AdminTextInputWidget
from django import forms
from turtle import textinput
from django.forms import ModelForm

#-------------------- IMPORTANDO ARCHIVOS DE APP ------------------------------
from .models import RegistroUnidad
from apps.general.views import *

#===========================================================================================
#|                                  Formulario Unidad                                      |
#===========================================================================================

# class FormUnidad(forms.Form):
#     codigo = forms.CharField(disabled=True, max_length=14, widget='class':'form-control-lg')
#     nombre = forms.CharField(max_length=2560, widget='class':'form-control-lg')
#     abreviatura= forms.CharField(max_length=16, widget='class':'form-control-lg')
#     tel_fijo= forms.CharField(max_length=8, widget='class':'form-control-lg')
#     correo= forms.EmailField(max_length=128, widget='class':'form-control-lg')
#     departamento= forms.Select(widget='class':'form-control-lg')
#     municipio=forms.Select(widget='class':'form-control-lg')
#     aldea=forms.Select(forms.DateField(widget=AdminDateWidget()))
#     en_funcionamiento=forms.Select(forms.DateField(widget=AdminDateWidget()))
#     en_funcionamiento_anio=forms.DateField()
#     usuario_creador=forms.CharField()

class UnidadForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     self.fields['usuario_creador'].widget = forms.HiddenInput()

    class Meta:
        model=RegistroUnidad    
        #fields = '__all__'
        
        fields = [
            'codigo',
            'nombre',
            'abreviatura',
            'tel_fijo',
            'extension',
            'correo',
            'departamento',
            'municipio',
            'aldea',
            #'dir_correspondencia'
            'en_funcionamiento',
            'en_funcionamiento_anio',
            'usuario_creador',
         
         ] 

        # # labels= { 
             
        # #     'codigo':'Codigo:',
        # #     'nombre':'Nombre:',
        # #     'abreviatura':'Abreviatura:',
        # #     'tel_fijo':'Telefono fijo:',
        # #     'extension':'Extension:',
        # #     'correo':'Correo:',
        # #     'departamento':'Departamento:',
        # #     'municipio':'Municipio:',
        # #     'aldea':'Aldea:',
        # #     'direccion_correspondencia':'Direccion correspondencia:',
        # #     'en_funcionamiento':'Esta en funcionamiento:',
        # #     'en_funcionamiento_anio':'Año que inicio funcionamiento :',
        # #     'usuario_creador':'usuario que lo crea:',
        # #  }

        widget = {
        #     'codigo': forms.TextInput(attrs={'class':'form-control-lg','placeholder':'Codigo'}),
        #     'nombre': forms.TextInput(attrs={'class': 'form-control-lg','placeholder':'Nombre de la unidad'}),
        #     'abreviatura': forms.TextInput(attrs={'class': 'form-control-lg','placeholder':'Abreviatura'}),
        #     'tel_fijo': forms.TextInput(attrs={'class': 'form-control-lg','placeholder':'2226-6200'}),
        #     'extension': forms.TextInput(attrs={'class': 'form-control-lg'}),
        #     'correo': forms.EmailInput(attrs={'class': 'form-contro-lg','placeholder':'ejemplo@se.gob.edu'}),
        #     'departamento': forms.Select(attrs={'class': 'form-control-lg'}),
        #     'municipio': forms.Select(attrs={'class': 'form-control form-round custom-select form-control-lg'}),
        #     'aldea': forms.Select(attrs={'class': 'form-control form-round custom-select form-control-lg'}),
        #     #'direccion_correspondencia':forms.TextInput(attrs={'class':'form-control-lg'}),
        #     'en_funcionamiento': forms.Select(attrs={'class': 'form-control form-round custom-select form-control-lg'}),
        #     'en_funcionamiento_anio': forms.DateField(widget=AdminDateWidget()), 
             'usuario_creador':forms.HiddenInput(),
        }


    #Departamento, Municipio
    #departamento, municipio

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['municipio'].queryset = Municipio.objects.none()

    #     if 'departamento' in self.data:
    #         try:
    #             departamento = int(self.data.get('departamento'))
    #             self.fields['municipio'].queryset = Municipio.objects.filter(departamento=departamento).order_by('nommbre')
    #         except (ValueError, TypeError):
    #             pass 
    #     elif self.instance.pk:
    #         self.fields['municipio'].queryset = self.instance.departamento.municipio_set.order_by('nommbre')