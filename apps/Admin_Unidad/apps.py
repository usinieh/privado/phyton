from django.apps import AppConfig


class AdminUnidadConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.Admin_Unidad'
