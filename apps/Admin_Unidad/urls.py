from django.urls import path

from .views import *
from .models import *


app_name = 'Admin_Unidad'

urlpatterns = [

#===========================================================================================
#|                              URLS DE ADMIN UNIDAD                                       |
#===========================================================================================

    #path('admin', views.cntUni, name= 'cuenta'),
    #path('agregar', UnidadCreateView, name= 'crear_momdelo'),
    #path('agregar', UnidadCreateView.as_view(), name= 'crear_momdelo'),

    path('agregar', index, name= 'agregar'),

#------------------------------- Agregar  ----------------------------------------------  

    path('guardar', procesar_formulario),

#------------------------------- Enlistar ----------------------------------------------    
    path('listar', ver_unidad),

#------------------------------- editar ------------------------------------------------ 
    path('editar', editar_unidad),
    path('edicion/<int:id>', edicion_unidad),

#------------------------------- dar de baja ------------------------------------------- 
    path('eliminar/<int:id>', eliminar_unidad),


#---------------------------vista de unidad individual ---------------------------------- 
    path('individual/<int:id>', individual_unidad),

]