from django.db import models
from apps.Admin_Cuenta.models import *
from django.contrib.auth.models import Permission, User, Group
from apps.Admin_Cuenta.choices import *

# Create your models here.

#===========================================================================================
#|                              MODELO REGISTRO DE UNIDAD                                  |
#===========================================================================================
 

class  RegistroUnidad(models.Model):
    codigo = models.CharField(max_length=14, unique=True, help_text=u'Código del Unidad', verbose_name=u'Código')
    nombre = models.CharField(max_length=256, help_text=u'Nombre completo de Unidad, segun acuerdo', verbose_name='Nombre')
    abreviatura = models.CharField(max_length=16, help_text=u'Abreviatura de nombre de Unidad', verbose_name='Abreviatura', null=True, blank=True)
    tel_fijo = models.CharField(max_length=8, help_text=u'Teléfono fijo del Unidad', verbose_name=u'Teléfono Fijo')
    extension=models.CharField(max_length=5,help_text=u'extension de la unidad',verbose_name='extension',default='0')
    correo = models.EmailField(max_length=128, help_text=u'Correo Electrónico del Unidad', null=True, blank=True)
    departamento = models.ForeignKey(Departamento,on_delete=models.CASCADE, help_text='Departamento donde esta ubicado el Unidad', verbose_name='Departamento')
    municipio = models.ForeignKey(Municipio,on_delete=models.CASCADE, help_text = 'Municipio donde se encuentra el Unidad',verbose_name='Municipio')
    aldea = models.ForeignKey(Aldea,on_delete=models.CASCADE, help_text = 'Aldea donde se encuentra el Unidad',verbose_name='Aldea', blank = True, null = True)
    #direccion_correspondencia= models.CharField(max_length=50, help_text=u'Direccion de la correspondencia de la Unidad', verbose_name=u'Direccion Correspondencia', default='Tegucigalpa M.D.C.')
    #dir_correspondencia= models.CharField(max_length=30,help_text=u'Direccion de la correspondencia de la Unidad',default='Tegucigalpa M.D.C.')
    en_funcionamiento = models.IntegerField(choices = BOOLEANO, help_text='Se encuentra en funcionamiento el centro',verbose_name='En Funcionamiento?')
    en_funcionamiento_anio = models.DateField(help_text=u'Año en que empezó a funcionar el Unidad',verbose_name=u'Año')
    nofuncionamiento_motivo = models.CharField(max_length=128, help_text='Motivo por el cual el Unidad no esta en funcionamiento',null = True, blank = True,verbose_name='Motivo')
    cambio_nombre = models.IntegerField(choices = BOOLEANO, help_text='Cambio de nombre el Unidad',verbose_name=u'Cambió de Nombre?',default='2')
    cambio_nombre_nombreanterior = models.CharField(max_length=256, help_text='Nombre anterior del Unidad',null = True, blank = True,verbose_name='Nombre Anterior')
    cambio_nombre_fechacambio = models.DateField(max_length = 10, help_text='Fecha en que cambio de nombre el Unidad', null = True, blank = True,verbose_name='Fecha')
    cambio_nombre_acuerdo = models.CharField(max_length=32, help_text=u'Numero de Acuerdo para el cambio de nombre', null=True, blank=True, verbose_name='No. Acuerdo')
    bloqueado = models.BooleanField(default=False)
    usuario_creador = models.ForeignKey(User,on_delete=models.CASCADE, related_name='centro_creador', null=True, blank=True)
    usuario_modifico = models.ForeignKey(User,on_delete=models.CASCADE, related_name='centro_modificador', null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, null=True, blank=True)
    
    def __str__(self):
            return self.nombre

    def __unicode__(self):
        return u'( %s ) - %s' % (self.codigo,self.nombre)

    class Meta:
        verbose_name = 'unidad'
        verbose_name_plural = 'unidades'
