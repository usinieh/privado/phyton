"""gestion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LoginView
from apps.accesopublico.views import vista,ingrasar,logoutUser
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('',vista, name='index'),

#===========================================================================================
#|                               URL PARA AUTENTICACION                                    |
#===========================================================================================   
    path('logout/', logoutUser, name= 'logout'),
    path('ingrasar/', ingrasar, name='login'),
    #path('ingrasar/',LoginView.as_view, {'template_naem: ingreso/login.html'}, name='login'),

#===========================================================================================
#|                           URL PARA APLICACIONES DJANGO                                  |
#===========================================================================================
    path('/', include(('apps.accesopublico.url','accesopublico'))),
    path('general/', include(('apps.general.urls','general'))),
    path('cuentas/', include(('apps.Admin_Cuenta.urls','cuentas'))),
    path('unidad/', include(('apps.Admin_Unidad.urls','unidad'))),
    path('dependencia/', include(('apps.Admin_Dependencia.urls','dependencia'))),
    path('correspondencia/', include(('apps.correspondencia.urls','correspondencia'))),
]

#===========================================================================================
#|                            Solo en debug (desarrollo)                                   |
#===========================================================================================

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
